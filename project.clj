(defproject scramblies "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]]
  :profiles {:dev {:dependencies [[org.clojure/test.check "0.10.0-alpha2"]]}
             :uberjar {:aot :all
                       :main scramblies.server}})
