(require '[ring.adapter.jetty :refer [run-jetty]])
(require '[ring.middleware.defaults :refer [wrap-defaults site-defaults]])
(require '[ring.util.response :refer [resource-response content-type]])
(require '[tailrecursion.ring-proxy :refer [wrap-proxy]])

(defn handler [req]
  (or
   (cond
     (= "/" (:uri req))
     (some-> (resource-response "index.html" {:root "public"})
             (content-type "text/html; charset=utf-8")))
   {:status 404
    :headers {"Content-Type" "text/html"}
    :body "Not found"}))

(run-jetty
 (wrap-defaults (wrap-proxy handler "/scramblies" "http://localhost:8080/scramblies") site-defaults)
 {:port 4000
  :join? false})