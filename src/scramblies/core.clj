(ns scramblies.core)

(defn- update-counters
  [counters c]
  (let [updated (case (counters c)
                  nil counters
                  1 (dissoc counters c)
                  (update counters c dec))]
    (if (empty? updated)
      (reduced updated)                                                         ;; perf optimization : stop reduction
      updated)))

;; note: the assignment is about strings but the algorithm applies to any ISeq
;; so I prefer to have something abstract at that level
(defn scramble?
  "Returns true if coll is a subset of match. Match can have duplicates.
   Otherwise returns false"
  [coll match]
  (and
    (>= (count coll) (count match))                                             ;; perf optimization : fail fast (will realize any lazy seq though)
    (empty? (reduce update-counters (frequencies match) coll))))
