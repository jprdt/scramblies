(ns scramblies.web.core
  (:require [goog.dom :as dom]
            [goog.events :as events]
            [cljs.reader :as reader])
  (:import [goog.events EventType]
           [goog.net XhrIo]))

(def endpoint-url (str js/window.location.protocol "//"
                       js/window.location.host
                       "/scramblies"))

(defn make-input
      [id]
      (doto (dom/createElement "input")
            (dom/setProperties #js {"id" id "name" id})))

(defn make-label
      [id label]
      (doto (dom/createElement "label")
            (dom/setProperties #js {"for" id})
            (dom/setTextContent label)))

(defn make-input-label
      [id label]
      (doto (dom/createElement "div")
            (dom/appendChild (make-label id label))
            (dom/appendChild (make-input id))))

(defn make-button
      [id label]
      (doto (dom/createElement "button")
            (dom/setProperties #js {"id" id "type" "button"})
            (dom/setTextContent label)))

(defn make-result
      [id]
      (doto (dom/createElement "p")
            (dom/setProperties #js {"id" id})))

(defn value [id]
      (-> (dom/getElement id)
          (.-value)))

(defn show-scramble-result [result-id result]
      (dom/setTextContent (dom/getElement result-id)
                          (str "result :" (:scramble? result))))

(defn error [result-id]
      (dom/setTextContent (dom/getElement result-id) "error"))

(defn scramble-callback [result-id]
      (fn [e]
          (let [target (.-target e)
                status (.getStatus target)]
               (case status
                     200 (show-scramble-result result-id (-> target .getResponse reader/read-string))
                     (error result-id)))))

(defn send-scramble [s-id match-id result-id endpoint-url]
      (fn [e]
          (XhrIo.send (str endpoint-url "?s=" (value s-id) "&match=" (value match-id))
                      (scramble-callback result-id))))

(defn make-app
      [mount-point]
      (let [s-id "scramblies-s"
            match-id "scramblies-match"
            result-id "scramblies-result"]
           (doto (dom/getElement "app")
                 (dom/appendChild (make-input-label s-id "s:"))
                 (dom/appendChild (make-input-label match-id "match:"))
                 (dom/appendChild (doto (make-button "scramblies-send" "Scramble?")
                                        (events/listen EventType.CLICK
                                                       (send-scramble s-id match-id result-id endpoint-url))))
                 (dom/appendChild (make-result result-id)))))

(defonce app (make-app "app"))
