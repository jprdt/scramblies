(ns scramblies.server
  (:require [ring.adapter.jetty :as jetty]
            [scramblies.services :as services]
            [ring.util.response :as ring-response]
            [ring.middleware.params :as ring-params]
            [ring.middleware.resource :as ring-resource]
            [ring.middleware.content-type :as ring-content-type]
            [ring.middleware.not-modified :as ring-not-modified])
  (:gen-class))


(defn body->edn
  [{:keys [body] :as response}]
  (if body
    (-> response
        (update :body pr-str)
        (ring-response/content-type "application/edn"))
    response))

(defn static-resources [handler]
  (-> handler
      (ring-resource/wrap-resource "public")
      (ring-content-type/wrap-content-type)
      (ring-not-modified/wrap-not-modified)))

(defn main-handler
  [request]
  (let [scramblies (ring-params/wrap-params services/scramblies-handler)]
    (-> request
        scramblies
        body->edn)))

(defonce server (atom nil))

(defn -main [port]
  (reset! server
          (jetty/run-jetty
            (static-resources #'main-handler)
            {:port  (Integer/parseInt port)
             :join? false})))
