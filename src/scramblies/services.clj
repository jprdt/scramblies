(ns scramblies.services
  (:require
    [ring.util.response :as ring-response]
    [scramblies.core :as scramblies]))

(defn valid [s] (and s (re-matches #"[a-z]*" s)))

(defn scramblies-handler
  [{:keys [params] :as request}]
  (let [s     (valid (params "s"))
        match (valid (params "match"))]
    (if (and s match)
      (ring-response/response
        {:s         s
         :match     match
         :scramble? (scramblies/scramble? s match)})
      {:status 400})))