# scramblies

A Clojure library designed to pass a test.

## Prerequisites 

Leiningen, clojure deps, and a JVM

## Build

Compile ClojureScript, then Clojure

```bash
clojure -m figwheel.main --compile-opts prod-compile-opts.edn --build-once scramblies
lein do test, uberjar
```

It results and uberjar whith the api, and the files to serve to browser (HTML, JS)

## Run (production mode)

java -jar target/scramblies*-standalone.jar 8080

Then go to [http://localhost:8080/index.html]()

## Run (dev mode)

Play with backend : 

`lein repl`
`(require 'scramblies.server)`
`(scramblies.server/-main "8080")`
`(stop @server)`

Play with frontend :

`clojure -i scripts/proxy.clj -m figwheel.main --build scramblies --repl`