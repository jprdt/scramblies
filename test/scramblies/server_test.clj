(ns scramblies.server-test
  (:require [clojure.test :refer :all])
  (:require [scramblies.server :as sut]))

(deftest main-handler-test
  (is (= {:body    "{:s \"azerty\", :match \"aze\", :scramble? true}"
          :headers {"Content-Type" "application/edn"}
          :status  200}
         (sut/main-handler {:query-string "s=azerty&match=aze"})))
  (is (= 400 (:status (sut/main-handler {:query-string "s=azerty"}))))
  (is (= 400 (:status (sut/main-handler {:query-string "match=azerty"}))))
  (is (= 400 (:status (sut/main-handler {:query-string "s=aze&match=aZerty"}))))
  (is (= 400 (:status (sut/main-handler {:query-string "s=aZe&match=azerty"})))))
