(ns scramblies.core-test
  (:require [clojure.test :refer :all]
            [scramblies.core :as sut]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.properties :as prop]))


(deftest scramble?-test
  (is (sut/scramble? "rekqodlw" "world"))
  (is (sut/scramble? "cedewaraaossoqqyt" "codewars"))
  (is (not (sut/scramble? "katas" "steak")))
  (is (sut/scramble? nil nil))
  (is (sut/scramble? #{1} nil))
  (is (sut/scramble? [1 3 4 6 3] [3 3 6]))
  (is (not (sut/scramble? nil #{1}))))

;; note : This is a bit overkill here for this simple assignment
(defspec scramble?-spec
  100
  (prop/for-all [coll (gen/list gen/char-alpha)]
    (let [truncated (drop (inc (rand-int (count coll))) coll)]
      (is (sut/scramble? coll coll))
      (is (sut/scramble? (shuffle coll) coll))
      (is (sut/scramble? (shuffle coll) truncated))
      (is (or (empty? coll)
              (not (sut/scramble? truncated coll)))))))